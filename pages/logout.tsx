import { Center, ChakraProvider } from "@chakra-ui/react";
import { Layout } from "components/Global";
import type { NextPage } from "next";
import router from "next/router";
import { Component } from "react";
import LayoutNoSignIn from '../components/Global/LayoutNoSignIn'
import Footer from "./footer";

export default function ThankYou() {
    return (
      <ChakraProvider>
        <LayoutNoSignIn>
          <Center my={250}>
            <h2>Hope to see you again soon!</h2>
          </Center>
          <Footer />
        </LayoutNoSignIn>
      </ChakraProvider>
    )
  };
