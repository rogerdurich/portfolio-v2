import { Layout } from 'components/Global'
import Link from 'next/link';
import { useUser } from '../lib/hooks'
import user from './api/user';


export default function Profile() {

  useUser({redirectTo: '/login'})
    
  return (
    <section>
      <h1>Profile</h1>

      {user && (
        <>
          <p>Your session:</p>
          <pre>{JSON.stringify(user, null, 2)}</pre>
          <p><Link href="/api/logout">Logout</Link></p>
        </>
      )}

      <style jsx>{`
        pre {
          white-space: pre-wrap;
          word-wrap: break-word;
        }
      `}</style>
    </section>
  )
}
