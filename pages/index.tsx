import { Box, Center, SimpleGrid } from "@chakra-ui/react";
import type { NextPage } from "next";
import { Waterpipe, options } from "components/Global";
import { BlogSection } from "../components/Blog";

const Home: NextPage = () => {
  return (
    <>
      <Box mt="-84px">
        <Waterpipe options={options} />
      </Box>
      <SimpleGrid
        w={"full"}
        position="relative"
        left={0}
        py={7}
        columns={1}
        gap={6}
      >
        <BlogSection />
      </SimpleGrid>
    </>
  );
};

export default Home;
