import Image from "next/image";
import {
  Box,
  Heading,
  Text,
  Stack,
  Avatar,
  SimpleGrid,
} from "@chakra-ui/react";
import { useUser } from "lib/hooks";


export async function getStaticProps() {
  const res = await fetch(process.env.API_URL, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${process.env.READ_TOKEN}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `
      query Posts {
        posts {
          id
          slug
          title
          blurb
          coverImage {
            url
          }
          author {
            name
          }
        }
      }`,
    }),
  });
  const posts = await res.json();
  const postArr = posts.data.posts;
  return {
    props: { posts: postArr },
  };
}

export default function Journal(props: { posts: any[] }) {
  useUser({ redirectTo: '/login' })
  // console.log(props.authors[0].name);
  return (
    <>
      <SimpleGrid
        mx={[null, null, null, null, null, "10%"]}
        position="relative"
        left={"0"}
        py={6}
        px={[2, 6]}
        columns={[null, 2, 2, 3, 4]}
        gap={6}
      >
        {props.posts.map((post) => (
          <Box
            key={post.id}
            maxW={'445px'}
            w={"full"}
            bg={"gray.900"}
            boxShadow={"2xl"}
            rounded={"md"}
            p={6}
            overflow={"hidden"}
          >
            <Box
              h={"210px"}
              bg={"gray.100"}
              mt={-6}
              mx={-6}
              mb={6}
              pos={"relative"}
            >
              <Image
                src={post.coverImage.url}
                alt="The cover image of the post"
                layout={"fill"}
              />
            </Box>
            <Stack>
              <Text
                color={"green.500"}
                textTransform={"uppercase"}
                fontWeight={800}
                fontSize={"sm"}
                letterSpacing={1.1}
              >
                Blog
              </Text>
              <Heading
                color={"gray.700"}
                fontSize={"2xl"}
                fontFamily={"body"}
              >
                <a href={post.slug}>{post.title}</a>
              </Heading>
              <Text color={"gray.500"}>{post.blurb}</Text>
            </Stack>
            <Stack mt={6} direction={"row"} spacing={4} align={"center"}>
              <Avatar
                src={"https://avatars0.githubusercontent.com/u/1164541?v=4"}
                alt={"Author"}
              />
              <Stack direction={"column"} spacing={0} fontSize={"sm"}>
                <Text fontWeight={600}>{post.author?.name}</Text>
                <Text color={"gray.500"}>Feb 08, 2021 · 6min read</Text>
              </Stack>
              )
            </Stack>
          </Box>
        ))}
      </SimpleGrid>
    </>
  );
}
