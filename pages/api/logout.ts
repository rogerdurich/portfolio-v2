import { magic } from '../../lib/magic'
import { removeTokenCookie } from '../../lib/auth-cookies'
import { getLoginSession } from '../../lib/auth'

export default async function logout(req, res) {

  try {
    await magic.user.logout()
    removeTokenCookie(res)
  } catch (error) {
    console.error(error)
  }

  res.writeHead(302, { Location: '/logout' })
  res.end()
}
