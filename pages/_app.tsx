import type { AppProps } from 'next/app'
import { Center, ChakraProvider } from '@chakra-ui/react'
import {Layout} from '../components/Global'
import { magic } from 'lib/magic'
import '../components/Global/styles.css'
import { useState, useEffect } from 'react'
import { UserContext } from '../context/UserContext'
import { AuthStateContext } from 'context/AuthStateContext'
import LayoutNoSignIn from 'components/Global/LayoutNoSignIn'
import router from 'next/router'
import { url } from 'inspector'
import Link from 'next/link'
import Footer from './footer'

export interface UserData {
  email?: string,
  loading?: boolean
  user?: null
}

function App({ Component, pageProps }: AppProps) {
  const [ user, setUser ] = useState<UserData | null>(null);

  useEffect(() => {
    setUser({ loading: true });
    magic.user.isLoggedIn().then((isLoggedIn) => {
      if (isLoggedIn) {
        magic.user.getMetadata().then((userData) => setUser(userData));
      } else {
        setUser({ user: null });
      }
    });
  }, []);


  if (Component.displayName === 'ThankYou') {
    return (
      <ChakraProvider>
        <LayoutNoSignIn>
        <Component as content-wrap {...pageProps}></Component>
          <Footer />
        </LayoutNoSignIn>
      </ChakraProvider>
    )
};


  return(
    <ChakraProvider>
      <UserContext.Provider value={[user, setUser]}>
      <AuthStateContext.Provider value={{}}>
        <Layout>
          <Component as content-wrap {...pageProps}></Component>
        </Layout>
        </AuthStateContext.Provider>
      </UserContext.Provider>
    </ChakraProvider>
  ) 
}

export default App
