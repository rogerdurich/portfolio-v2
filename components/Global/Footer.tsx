import { Box, ButtonGroup, Flex, IconButton, Stack, Text } from '@chakra-ui/react'
import * as React from 'react'
import { FaGithub, FaLinkedin, FaTwitter } from 'react-icons/fa'
import { Logo } from '../Global/Logo'

export default function Footer(props): JSX.Element {
    return ( 
        <Flex 
            mx={[null, null, null, null, null, '5%']} 
            flex={{ base: 1 }}
            justify={{ base: 'center', md: 'start' }}
            className={'footer'}
            py={{ base: 2 }}
            px={{ base: 4 }}
            >
        <Stack>
          <Stack direction="row" align="center">
            <Logo />
            </Stack>
            <Stack direction="row" align="center">
            <ButtonGroup variant="ghost">
              <IconButton
                as="a"
                href="#"
                aria-label="LinkedIn"
                icon={<FaLinkedin fontSize="1.25rem" />}
              />
              <IconButton as="a" href="#" aria-label="GitHub" icon={<FaGithub fontSize="1.25rem" />} />
              <IconButton
                as="a"
                href="#"
                aria-label="Twitter"
                icon={<FaTwitter fontSize="1.25rem" />}
              />
            </ButtonGroup>
          </Stack>
          <Text px={2} fontSize="sm" color="subtle">
            &copy; {new Date().getFullYear()} Roger Urich. All rights reserved.
          </Text>
        </Stack>
      </Flex>
    )
}