import Navbar from './NavigationNoSignIn'
import Head from 'next/head'
import Footer from './Footer'

export default function Layout({ children }) {
  
  const navBarStyle = {
    top: 0
 }

  return (
    <>
      <Head>
        <title>Roger&#39;s Little Corner of the Internet</title>
        {/* <meta name="viewport" content="initial-scale=1.0, width=device-width" /> */}
        <meta name="description" content="A place for me to share thoughts, ideas, and even some personal stuff for friends and close colleagues" />      
      </Head>
          <div className='page-container'>
          <div style={navBarStyle}>
          <Navbar  />
          </div>
            <main className='content-wrap'>{children}</main>
          <Footer />
        </div>
    </>
  )
}