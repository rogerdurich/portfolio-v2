import { Button, Link } from '@chakra-ui/react'
import { AuthStateContext, useAuthState } from 'context/AuthStateContext'
import { magic } from 'lib/magic';
import { useEffect, useState } from 'react'

export default function LoginButton() {
    const [ url, setUrl ] = useState('');
    const [ title, setTitle ] = useState('Checking Status');

    useEffect(() => {
        magic.user.isLoggedIn().then((isLoggedIn) => {
            if (isLoggedIn) {
                setUrl('/api/logout');
                setTitle('Sign Out');
            } else {
                setUrl('/login');
                setTitle('Sign In');
            }
          });
        }, );

    return (
        <Link href={url}>
            <Button
                display={{ base: 'none', md: 'inline-flex' }}
                fontSize={'md'}
                fontWeight={600}
                color={'white'}
                bg={'pink.400'}
                _hover={{
                bg: 'pink.300',
                }}>
                {title}
            </Button>
        </Link>
    )
}