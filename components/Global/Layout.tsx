import Navbar from './Navigation'
import Head from 'next/head'
import Footer from './Footer'

export default function Layout({ children }) {

  return (
    <>
      <Head>
        <title>Roger&#39;s Little Corner of the Internet</title>
        {/* <meta name="viewport" content="initial-scale=1.0, width=device-width" /> */}
        <meta name="description" content="A place for me to share thoughts, ideas, and even some personal stuff for friends and close colleagues" />      
      </Head>
          <div className='page-container'>
          <Navbar />
            <main className='content-wrap'>{children}</main>
          <Footer />
        </div>
    </>
  )
}