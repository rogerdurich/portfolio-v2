import { BoxProps } from "@chakra-ui/react";

export interface BlogData {
    type: 'article' | 'webinar' | 'video'
    tags: string[]
    title: string
    description: string
    image: string
    href?: string
    author?: {
      name: string
      image: string
      title: string
    }
  }
  
export interface BlogCardProps extends BoxProps {
    data: BlogData
  }