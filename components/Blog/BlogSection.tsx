import { Box, Heading, SimpleGrid, Text, useColorModeValue as mode } from '@chakra-ui/react'
import { BlogCard } from '.'
import { data } from '../../mock_data/blog_data'

export default function Blog() {
  return (
    <Box position='relative' left={'0'} bg={mode('gray.50', 'inherit')} as="section" py="24" w="full" my={-7}>
      <Box maxW={{ base: 'xl', md: '2xl', lg: '7xl' }} mx="auto" px={{ base: '6', md: '8' }}>
        <Box textAlign="center" maxW="md" mx="auto">
          <Heading size="2xl" fontWeight="extrabold" letterSpacing="tight">
            Latest Posts
          </Heading>
          <Text mt="4" fontSize="lg" color={mode('gray.600', 'gray.400')}>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsa libero labore natus
            atque, ducimus sed.
          </Text>
        </Box>
        <SimpleGrid mt="14" columns={{ base: 1, lg: 3 }} spacing="14">
          {data.map((item, index) => (
            <BlogCard key={index} data={item} />
          ))}
        </SimpleGrid>
      </Box>
    </Box>
  )
}
