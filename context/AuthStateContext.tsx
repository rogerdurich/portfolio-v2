import { createContext, useContext, useState } from 'react'

export const AuthStateContext = createContext(undefined)

export function AuthStateProvider({ children }) {
    const [title, setTitle] = useState({})
    const [url, setUrl] = useState({})
    return (
        <AuthStateContext.Provider
            value={{
                title,
                setTitle,
                url,
                setUrl
            }}
        >
            {children}
        </AuthStateContext.Provider>
    )
}

export function useAuthState({}) {
    const context = useContext(AuthStateContext)

    if (!context)
        throw new Error('useAuthState must be used inside an `AuthStateProvider`')

    return context;
}