require("dotenv").config();

module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['media.graphcms.com','media.graphassets.com']
  },
  env: {
    REDIRECT_URI: process.env.REDIRECT_URI
  }
}
